import Vue from 'vue'
import App from './App'
import store from './store'
Vue.config.productionTip = false

import mta from './libs/mta_analysis.js'
Vue.prototype.$mta = mta

// 后端地址
// const serviceUrl = 'http://192.168.43.123:9999';
const serviceUrl = 'https://www.pigfun.top';
const cdnPath = 'https://www.pigfun.top/cdn/';
// websocket地址
// const serverWss = "ws://192.168.43.123:9999/wss/im/";
const serverWss = "wss://www.pigfun.top/wss/im/";

// CDN图片资源
const cdn = {
	cdnPath: cdnPath,
	head_example: cdnPath + 'common/head_example.png',
	add_main_tip: cdnPath + 'common/add_main_tip.png',
	place_tip: cdnPath + 'common/place_tip.png',
	note_tip: cdnPath + 'common/note_tip.png',
	share_tip: cdnPath + 'common/share_tip.jpg',
	leader_tip: cdnPath + 'common/leader_tip.png',
	plaza_bg_example: cdnPath + 'common/plaza_bg.jpg',
	actCoverImg: cdnPath + 'actCoverImg/',
	noteCoverImg:cdnPath + 'noteCoverImg/',
	merCoverImg:cdnPath + 'merchantCoverImg/',
	appAvatar: cdnPath + "appAvatar/",
	actType: cdnPath + "actType/",
	actGroupQR: cdnPath + "actGroupQR/",
	ad: cdnPath + "ad/",
	photo: cdnPath + "photo/",
	faceImg: cdnPath + "faceImg/",
	groupTypeCover: cdnPath + "groupTypeCover/",
	groupCover: cdnPath + "groupCover/",
	groupQR: cdnPath + "groupQR/",
	groupDesc: cdnPath + "groupDesc/",
	plazzPhoto: cdnPath + "plazzPhoto/",
	common: cdnPath + "common/"
}

// 接口列表
const api = {
	pageSize: 10,
	// card左滑右滑
	cardPageSize: 8,
	getHomeData: '/api/home/getHomeData.short',
	listHomeAct: '/api/act/listHomeAct.short',
	listNote: '/api/note/listNote.short',
	getHotSearchList: '/api/home/getHotSearchList',
	pageActByActTypeId: '/api/act/pageActByActTypeId.short',
	searchByKey: '/api/searchRec/searchByKey',
	yetJoinActMember: '/api/member/yetJoinActMember',
	pageByNear: '/api/member/pageByNear.short',
	getMemberListRecommend: '/auth/api/member/getMemberListRecommend',
	pagePlazaContent: '/api/plazaContent/pagePlazaContent',
	deletePlaza: '/auth/api/plazaContent/deletePlaza',
	pagePlazaContentById: '/api/plazaContent/pagePlazaContentById',
	insertPlazaComment: '/auth/api/plazaComment/insertPlazaComment',
	pageGroup: '/api/group/pageGroup.short',
	getMyJoinGroupList: '/auth/api/group/getMyJoinGroupList',
	pageGroupByMe: '/auth/api/group/pageGroupByMe',
	pageByNearMember: '/api/member/pageByNear.short',
	pageActNear: '/api/act/pageActNear.short',
	pageToMyMsg: '/auth/api/message/pageToMyMsg',
	pageToChat: '/auth/api/message/page',
	pageToMyJoinAct: '/auth/api/act/pageToMyJoinAct',
	pageToMePublish: '/auth/api/act/pageToMePublish'
}

const tui = {
	toast: function(text, duration, success) {
		uni.showToast({
			title: text,
			icon: success ? 'success' : 'none',
			duration: duration || 2000
		})
	},
	constNum: function() {
		const res = uni.getSystemInfoSync();
		return res.platform.toLocaleLowerCase() == "android" ? 300 : 0;
	},
	px: function(num) {
		return uni.upx2px(num) + 'px';
	},
	interfaceUrl: function() {
		//接口地址
		// return "http://192.168.8.161:8888/";
		return serviceUrl;
	},
	request: function(url, postData, method, type, hideLoading) {
		//接口请求
		if (!hideLoading) {
			uni.showLoading({
				mask: true,
				title: '玩命加载中...'
			})
		}
		return new Promise((resolve, reject) => {
			uni.request({
				url: this.interfaceUrl() + url,
				data: postData,
				header: {
					'content-type': type ? 'application/x-www-form-urlencoded' : 'application/json',
					'Authorization': this.getToken()
					// 是否启用跨域保护
					// 'security': 1
				},
				method: method, //'GET','POST'
				dataType: 'json',
				success: (res) => {
					!hideLoading && uni.hideLoading()
					if (res.data && res.data.code === 301 && !postData.modalAbate) {
						// 清空原过期登录信息
						// store.mutations.logout();
						this.toLoginTip();
					} else {
						resolve(res.data)
					}
				},
				fail: (res) => {
					if (!hideLoading) {
						this.toast("网络不给力，请稍后再试~")
						uni.hideLoading()
					}
					reject(res)
				}
			})
		})
	},
	uploadFile: function(url, src, hideLoading) {
		const that = this

		if (hideLoading == undefined || hideLoading == null || hideLoading == '') {
			// 默认显示加载中
			hideLoading = false;
		}
		if (!hideLoading) {
			uni.showLoading({
				mask: true,
				title: '玩命加载中...'
			})
		}
		return new Promise((resolve, reject) => {
			const uploadTask = uni.uploadFile({
				url: this.interfaceUrl() + url,
				filePath: src,
				name: 'file',
				header: {
					// 'content-type': 'multipart/form-data',
					// 'content-type': type ? 'application/x-www-form-urlencoded' : 'application/json',
					'Authorization': this.getToken(),
					// 'Access-Control-Allow-Origin':'*'
				},
				formData: {},
				success: function(res) {
					/* uni.hideLoading();
					resolve(JSON.parse(res.data)); */

					!hideLoading && uni.hideLoading();
					let data = JSON.parse(res.data);
					if (data && data.code === 301) {
						this.toLoginTip();
					} else {
						resolve(JSON.parse(res.data));
					}

					/* 					let d = JSON.parse(res.data)
										if (d.code === 1) {
											let fileObj = JSON.parse(d.data)[0];
											//文件上传成功后把图片路径数据提交到服务器，数据提交成功后，再进行下张图片的上传
											resolve(fileObj)
										} else {
											that.toast(res.message);
										} */
				},
				fail: function(res) {
					// reject(res)
					// uni.hideLoading();
					// that.toast(res.message);

					if (!hideLoading) {
						this.toast("网络不给力，请稍后再试~")
						uni.hideLoading()
					}
					reject(res)
				}
			})
		})
	},
	geetWeek: function(dateStr) {
		let num = new Date(dateStr).getDay();
		if (num == 0) {
			return '周日'
		} else if (num == 1) {
			return '周一'
		} else if (num == 2) {
			return '周二'
		} else if (num == 3) {
			return '周三'
		} else if (num == 4) {
			return '周四'
		} else if (num == 5) {
			return '周五'
		} else if (num == 6) {
			return '周六'
		}
	},
	isBlank: function(str) {
		if (str == undefined || str == null || str == '') {
			return true;
		} else {
			return false;
		}
	},
	copy: function(text) {
		uni.setClipboardData({
			data: text,
			success(res) {
				uni.getClipboardData({
					success(res) {
						uni.showToast('复制成功');
					}
				});
			}
		});
	},
	getPic: function(headPic){
		if(headPic!=undefined){
			if(headPic.indexOf("http")>=0){
				return headPic;
			}else{
				return cdn["appAvatar"]+headPic;
			}
		}
	},
	getIncomeStateDesc(state) {
		// 我发布的聚会状态
		if (state == 0) {
			return '未通过';
		} else if (state == 1) {
			return '已发放';
		} else if (state == 2) {
			return '待发放';
		}else{
			return '待发放';
		}
	},
	getCommTakeStateDesc(state) {
		// 我发布的聚会状态
		if (state == 0) {
			return '未生效';
		} else if (state == 1) {
			return '已生效';
		}
	},
	getCommGiveStateDesc(state) {
		// 我发布的聚会状态
		if (state == 0) {
			return '待发放';
		} else if (state == 1) {
			return '已发放';
		}
	},
	getIncomeStateColor(state) {
		// 我发布的聚会状态
		if (state == 2) {
			return 'orange';
		} else if (state == 1) {
			return '#19be6b';
		} else if (state == 0) {
			return 'red';
		}else{
			return 'orange';
		}
	},
	copy: function(text) {
		const that = this;
		uni.setClipboardData({
			data: text,
			success(res) {
				uni.getClipboardData({
					success(res) {
						//that.tui.toast("邮箱已复制", 2000, true)
					}
				});
			}
		});
	},
	// 照片压缩设置大小
	reSizeImg: function(src, width, height, callback) {
		uni.getImageInfo({
			src,
			success(res) {
				console.log('压缩前', res)
				let canvasWidth = res.width //图片原始长宽
				let canvasHeight = res.height
				let img = new Image()
				img.src = res.path
				let canvas = document.createElement('canvas');
				let ctx = canvas.getContext('2d');
				// 未传则默认缩放
				if (width == null || height == null) {
					canvas.width = canvasWidth * 0.6;
					canvas.height = canvasHeight * 0.6;
				} else {
					canvas.width = width;
					canvas.height = height;
				}
				ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
				canvas.toBlob(function(fileSrc) {
					let imgSrc = window.URL.createObjectURL(fileSrc)
					console.log('压缩后', imgSrc);
					return callback(imgSrc);
				})
			}
		})
	},
	requestSubscribeMessage(type){
		// #ifdef MP-WEIXIN || MP-BAIDU || MP-QQ
		let tmplIds=['klMmSikUvV2McCVdQ1sNg3nr6DwkIS9PrYFsLnTxRzk','0D0qpfwNbgXyWG6cLitwnAzoLhs35V-M-DZCoouLSD4','3G0dYC7hFED7lvqGzBL2jrSAXOnQzTw0Ha31UnhzmDQ'];
		if(type=='join'){
			// 申请加入审核通过,新人加入通知，新消息提醒
			tmplIds=['klMmSikUvV2McCVdQ1sNg3nr6DwkIS9PrYFsLnTxRzk','0D0qpfwNbgXyWG6cLitwnAzoLhs35V-M-DZCoouLSD4','3G0dYC7hFED7lvqGzBL2jrSAXOnQzTw0Ha31UnhzmDQ'];
		}else if(type=='pub'){
			// 二维码到期提醒，新人加入通知，新消息提醒
			tmplIds=['G-vqYe0hE6bgFWaKO_RlQCemMy5jH2i5BrCBfpUJn3E','0D0qpfwNbgXyWG6cLitwnAzoLhs35V-M-DZCoouLSD4','3G0dYC7hFED7lvqGzBL2jrSAXOnQzTw0Ha31UnhzmDQ'];
		}
		// 订阅消息
		uni.requestSubscribeMessage({
		  tmplIds: tmplIds,
		  success (res) {
			  console.log(JSON.stringify(res));
		  }
		});
		// #endif
	},
	setToken: function(token) {
		uni.setStorageSync("token", token)
	},
	getToken() {
		return uni.getStorageSync("token")
	},
	isLogin: function() {
		return uni.getStorageSync("token") ? true : false
	},
	getQQmapsdkKey:function(){
		return 'CLDBZ-CENYD-XMG42-PUZHD-WYRMO-JJBAU';
		// juqu
		// CLDBZ-CENYD-XMG42-PUZHD-WYRMO-JJBAU
		// 第三方
		// W4WBZ-TUD65-IDAIR-QPM36-HMFQ5-CGBZP
	},
	loginTip(isShowCancel){
		uni.showModal({
			title: '登录',
			content: '您还未登录，请先登录',
			showCancel: isShowCancel,
			confirmColor: "#5677FC",
			confirmText: '确定',
			success(e) {
				if (e.confirm) {
					uni.navigateTo({
						url: '/pages/login/login'
					})
				} else if (e.cancel) {
					/* uni.navigateBack({
						delta: 1
					}); */
				}
			}
		})
	},
	toLoginTip(){
		this.loginTip(true);
	},
	toIndexAndTip() {
		let that=this;
		uni.showModal({
			// title: '提示',
			content: '去查看更多精彩',
			showCancel: true,
			confirmColor: '#5677fc',
			confirmText: '去查看',
			success(res) {
				if (res.confirm) {
					that.toHome();
				} else if (res.cancel) {
					// console.log('用户点击取消');
				}
			}
		});
	},
	toHome(){
		uni.switchTab({
		    url: '/pages/home/index'
		});
	}
}

Vue.prototype.tui = tui
Vue.prototype.$cdn = cdn
Vue.prototype.$api = api
Vue.prototype.$serverWss = serverWss
Vue.prototype.$eventHub = Vue.prototype.$eventHub || new Vue()
Vue.prototype.$store = store
App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
